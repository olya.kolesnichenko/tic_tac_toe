function View() {
    var self = this;

    self.controls = {
        x: document.getElementById("x"),
        o: document.getElementById("o"),
        msg: document.getElementById("message"),
        container: $("#container")
    };


    self.whoseStep = function (who) {
        if (who == 1)
            self.controls.msg.innerHTML = '<p>Ваш ход</p>';

        else
            self.controls.msg.innerHTML = '<p>Компьютер ходит</p>';

    };
    self.humanWin = function () {

        disabled();
        $(".disabled").append($('<p>Вы победили!!!</p>')).append("<img src='img/cat_lose.jpg'/>");

    };

    self.pcWin = function () {
        disabled();
        $(".disabled").append($('<p>Компьютер победил!!!</p>')).append("<img src='img/cat_win.jpg'/>");


    };
    self.tie = function () {
        disabled();
        $(".disabled").append($('<p>Ничья</p>'));


    };
    self.newField = function () {
        self.controls.container.empty();
        var $field = $('<div>').attr("id", "field");

        console.log($field);
        for (var i = 0; i < 9; i++) {
            var $newCell = $('<div>').attr("id", i).addClass('cell');

            $field.append($newCell);
        }
        i = 0;

        for (var k = 0; k < 3; k++) {
            for (var j = 0; j < 3; j++) {
                $field.children().eq(i).attr("data-i", k).attr("data-j", j);
                i++;
            }
        }
        console.log($field);
        self.controls.container.append($field);

    };
    function disabled() {
        self.controls.msg.innerHTML = '';
        $("#field").append($('<div>').addClass('disabled'));
    }

}