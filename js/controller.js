function Controller(view, model) {
    //1 - human step
    //0 - PC step

    // 7 - 0
    // 11 - x
    var queue = 0;

    var human = {
        sign: "",
        number: 0,
        color: ""
    };
    var pc = {
        sign: "",
        number: 0,
        color: ""
    };

    var fields = [];
    var arrField = [];
    var i = 0, j = 0;


    view.controls.x.addEventListener('click', chooseSign);
    view.controls.o.addEventListener('click', chooseSign);
    view.controls.container.click(checkCell);


    function chooseSign(e) {
        if (e.target.id === 'x') {
            queue = 1;

            pc.sign = "o";
            pc.num = 7;
            pc.color = "red";

            human.num = 11;
            human.color = "blue";


        }
        else {

            pc.sign = "x";
            pc.num = 11;
            pc.color = "blue";

            human.num = 7;
            human.color = "red";

        }
        human.sign = e.target.id;

        console.log(queue);

        newGame();

    }

    function newGame() {

        view.newField();

        arrField = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

        for (var i = 0; i < 9; i++) {

            var cell = document.getElementById(i);
            console.log(cell);
            fields.push(cell);
        }
        console.log(fields);
        if (queue == 0) {
            stepPC();
        }
        view.whoseStep(queue);

    }

    function checkCell(e) {
        console.log(e.target);
        if (!$(e.target).hasClass('cell') || queue == 0 || $(e.target).data('sign'))
            return;

        var id = e.target.id;
        selectCell(id, human);

        console.log(arrField);
        if (!check()) {

            view.whoseStep(queue);
            setTimeout(stepPC, 1000);
        }
    }

    function stepPC() {
        if (queue == 1)
            return;

        else if (!fields.some(checkData)) {
            selectCell(4, pc);
        }
        else {

            if (closeToVictory(pc) >= 0) {
                selectCell(closeToVictory(pc), pc);
            }
            else if (closeToVictory(human) >= 0) {
                selectCell(closeToVictory(human), pc);
            }
            else {
                var priorities = [4, 0, 2, 6, 8, 1, 3, 5, 7];

                for (var i = 0; i < priorities.length; i++) {
                    if (!$("#" + priorities[i]).data('sign')) {
                        console.log("id = " + priorities[i]);
                        selectCell(priorities[i], pc);
                        break;
                    }
                }
            }
        }

        if (!check()) {

            view.whoseStep(queue);
        }

    }

    function check() {

        if (checkVictory(pc.num)) {
            view.pcWin();
            return true;
        }
        else if (checkVictory(human.num)) {
            view.humanWin();
            return true;
        }
        else if (checkTie()) {
            view.tie();
            return true;
        }
        else
            return false;
    }

    function checkData(elem) {
        return $(elem).data('sign');
    }

    function checkTie() {
        if (fields.every(checkData))
            return true;
        else
            return false;
    }

    function checkVictory(num) {
        if (rowSum(0) == num * 3 || rowSum(1) == num * 3 || rowSum(2) == num * 3 || diag2Sum() == num * 3 ||
            colSum(0) == num * 3 || colSum(1) == num * 3 || colSum(2) == num * 3 || diag1Sum() == num * 3)
            return true;
        else
            return false;
    }

    function rowSum(index) {
        var sum = 0;
        for (var i = 0; i < 3; i++) {
            sum += arrField[index][i];
        }
        return sum;
    }

    function colSum(index) {
        var sum = 0;
        for (var i = 0; i < 3; i++) {
            sum += arrField[i][index];
        }
        return sum;
    }

    function diag1Sum() {
        var sum = 0;
        sum = arrField[0][0] + arrField[1][1] + arrField[2][2];
        return sum;
    }

    function diag2Sum() {
        var sum = 0;
        sum = arrField[2][0] + arrField[1][1] + arrField[0][2];
        return sum;
    }

    function closeToVictory(who) {
        if (closeToVictoryRow(who.num) >= 0) {
            return checkCellInRow(closeToVictoryRow(who.num));
        }
        if (closeToVictoryCol(who.num) >= 0) {
            return checkCellInCol(closeToVictoryCol(who.num));
        }
        if (closeToVictoryDiag(who.num) > 0) {
            return checkCellInDiag(closeToVictoryDiag(who.num));
        }
    }

    function checkCellInRow(index) {
        var j = 0;
        for (j = 0; j < 3; j++) {
            if (arrField[index][j] == 0) {
                return index * 3 + j;
            }
        }
        return -1;
    }

    function checkCellInCol(index) {
        var i = 0;
        for (i = 0; i < 3; i++) {
            if (arrField[i][index] == 0) {
                return i * 3 + index;
            }
        }
        return -1;
    }

    function checkCellInDiag(index) {
        if (index == 1) {
            for (var i = 0; i < 3; i++) {
                if (arrField[i][i] == 0) {
                    return i * 3 + i;
                }
            }
        }
        if (index == 2) {
            for (var j = 0; j < 3; j++) {
                if (arrField[j][3 - j - 1] == 0) {
                    return j * 3 + (3 - j - 1);

                }
            }
        }
        return -1;
    }

    function closeToVictoryRow(num) {
        var i = -1;
        for (i = 0; i < 3; i++) {
            if (rowSum(i) == num * 2) {
                return i;
            }
        }
    }

    function closeToVictoryCol(num) {
        var i = -1;
        for (i = 0; i < 3; i++) {
            if (colSum(i) == num * 2) {
                return i;
            }
        }
    }

    function closeToVictoryDiag(num) {

        if (diag1Sum(i) == num * 2) {
            return 1;
        }
        if (diag2Sum(i) == num * 2) {
            return 2;
        }
    }

    function selectCell(id, who) {

        var $currentCell = $("#" + id);

        if ($currentCell.data('sign'))
            return;

        $currentCell.css("background-color", who.color);
        $currentCell.attr("data-sign", who.sign);
        $currentCell.text(who.sign);
        i = $currentCell.data('i');
        j = $currentCell.data('j');
        arrField[i][j] = who.num;
        i = j = 0;
        console.log(arrField);
        queue = 1 - queue;
        view.whoseStep(queue);

    }
}